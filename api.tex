\newpage
\section{API}
\label{api}

The CryptoCore exposes an RS232 API, which you can call over a UART connection. For example, you could send API calls through a device that's connected to the CryptoCore over USB.

\subsection{setFlags}

Sets the given flags.

The following flags are available:
\begin{itemize}
\item keepSeedInRAM: Reads the seed from the secure element and caches it in RAM. (Caching the seed saves 1-2 seconds for each request of the seed.)
\item debugRS232Output: Enables debugging output on the SWD line or RS232.
\end{itemize}

Before you can set the keepSeedInRAM flag, you must initalize the secure element by calling the `initSecureElement' command. 

\textbf{Parameters}

\begin{tabular}{|c|c|c|}
\hline
Parameter & Type & Description \\
\hline
flags & object & The name/value pairs of the flags that you want to set \\
\hline  
\end{tabular}

\textbf{Example Request}

\begin{lstlisting}[language=json]
{
    "command":"setFlags",
    "flags":{
        "keepSeedInRAM":true
    }
}
\end{lstlisting}

\textbf{Example Responses}

\begin{lstlisting}[language=json]
{
    "code": 200,
    "command":"setFlags",
    "duration": 250
}
\end{lstlisting}

\begin{lstlisting}[language=json]
{
    "code": 400,
    "command":"setFlags",
    "error": "Error message"
}
\end{lstlisting}

\subsection{testHardwareAcceleration}

Tests the hardware acceleration on the ICCFPGA module.

The following functions are tested and compared with the results of unaccelerated functions:
\begin{itemize}
 \item Type conversions (bytes/trytes and bytes/trits)
 \item Hashing (proof of work, Curl, Keccak384, and Troika)
\end{itemize}

\textbf{Example Request}

\begin{lstlisting}[language=json]
{
    "command":"testHardwareAcceleration"
}
\end{lstlisting}

\textbf{Example Responses}

\begin{lstlisting}[language=json]
{
    "code": 200,
    "command":"testHardwareAcceleration"
    "bytesToTritsSingle":"pass",
    "tritsToBytesSingle":"pass",
    "pow":"pass",
    "keccak384":"pass",
    "bytesToTritsRandomRepeated":"pass",
    "tritsToBytesRandomRepeated":"pass",
    "trytesToBigintRandomRepeated":"pass",
    "bigintToTrytesRandomRepeated":"pass",
    "troika":"pass",
    "curl":"pass",
    "duration":1867
}
\end{lstlisting}

\begin{lstlisting}[language=json]
{
    "code": 400,
    "command":"testHardwareAcceleration"
    "error": "Error message"
}
\end{lstlisting}

\subsection{generateRandomSeed}

Generates a random seed and stores it in one of eight available memory addresses in the secure element.

Before you can call this command, you must initalize the secure element by calling the `initSecureElement' command.

\textbf{Parameters}

\begin{tabular}{|c|c|c|}
\hline
Parameter & Type & Description \\
\hline
key & integer & An integer between 0 and 7, which specifies the memory address in which to save the seed \\
\hline

\end{tabular}

\textbf{Example Request}

\begin{lstlisting}[language=json]
{
    "command":"generateRandomSeed",
    "key": 0
}
\end{lstlisting}

\textbf{Example Responses}

\begin{lstlisting}[language=json]
{
    "code": 200,
    "command":"generateRandomSeed",
    "duration":1800
}
\end{lstlisting}

\begin{lstlisting}[language=json]
{
    "code": 400,
    "command":"generateRandomSeed",
    "error": "Error message"
}
\end{lstlisting}

\subsection{generateAddress}

Generates addresses for the seed in the secure element's given key.

Before you can call this command, you must initalize the secure element by calling the `initSecureElement' command. 

\textbf{Parameters}

\begin{tabular}{|c|c|c|}
\hline
Parameter & Type & Description \\
\hline
key & integer & The memory address of the seed from which you want to derive the address \\
firstIndex & integer & The address index from which to start generating addresses \\
number & integer & The number of addresses to generate, starting from the first index \\
security & integer & The security level of the address that you want to generate \\
\hline

\end{tabular}

\textbf{Example Request}

\begin{lstlisting}[language=json]
{
    "command":"generateAddress",
    "key": 0,
    "firstIndex": 0,
    "number": 10,
    "security": 2
}
\end{lstlisting}

\textbf{Example Responses}

\begin{lstlisting}[language=json]
{
    "code": 200,
    "command":"generateAddress",
    "trytes": ["....","....",...],
    "duration": 1800
}
\end{lstlisting}

\begin{lstlisting}[language=json]
{
    "code": 400,
    "command":"generateAddress",
    "error": "Error message"
}
\end{lstlisting}

\subsection{attachToTangle}

Chains the transactions into a bundle, using the trunkTransaction and branchTransaction parameters, and does proof of work on all of them, using the given minimum weight magnitude.

This command can do proof of work for a bundle that contains up to eight transactions. If you want to do proof of work for larger bundles in a single command, you can add the branch transaction hash, trunk transaction hash, and timestamp to the transaction trytes yourself before passing them to the `doPow` command.

\textbf{Parameters}

\begin{tabular}{|c|c|c|}
\hline
Parameter & Type & Description \\
\hline
trunkTransaction & string & Trunk transaction hash to use to attach the bundle to the Tangle \\
branchTransaction & string & Branch transaction hash to use to attach the bundle to the Tangle \\
minWeightMagnitude & integer & The minimum weight magnitude to use during proof of work \\
timestamp & integer & A Unix epoch timestamp to add to the transaction's `timestamp' fields \\
trytes & array of strings & Transaction trytes of up to eight transactions in a bundle \\
\hline

\end{tabular}

\textbf{Example Request}

\begin{lstlisting}[language=json]
{
    "command":"attachToTangle",
    "trunkTransaction": "...",
    "branchTransaction": "...",
    "minWeightMagnitude": 14,
    "timestamp": 1552571227826,
    "trytes": ["....", "....", ...]
}
\end{lstlisting}

\textbf{Example Responses}

\begin{lstlisting}[language=json]
{
    "code": 200,
    "command":"attachToTangle",
    "trytes": ["....","....",...],
    "duration": 1800
}
\end{lstlisting}

\begin{lstlisting}[language=json]
{
    "code": 400,
    "command":"attachToTangle",
    "error": "Error message"
}
\end{lstlisting}

\subsection{doPow}

Does proof of work on an array of transaction trytes.

This command can do proof of work for up to 10 transactions at once.

\textbf{Parameters}

\begin{tabular}{|c|c|c|}
\hline
Parameter & Type & Description \\
\hline
minWeightMagnitude & integer & The minimum weight magnitude to use during proof of work \\
trytes & array of strings & Transaction trytes of the transactions \\
\hline

\end{tabular}


\textbf{Example Request}

\begin{lstlisting}[language=json]
{
    "command":"doPow",
    "minWeightMagnitude": 14,
    "trytes": ["....", "....", ...]
}
\end{lstlisting}

\textbf{Example Responses}

\begin{lstlisting}[language=json]
{
    "code": 200,
    "command":"doPow",
    "trytes": ["....","....",...],
    "duration": 1800
}
\end{lstlisting}

\begin{lstlisting}[language=json]
{
    "code": 400,
    "command":"doPow",
    "error": "Error message"
}
\end{lstlisting}

\subsection{signTransaction}

Signs a single input transaction, using the seed in the secure element's given key.

Before you can call this command, you need to do the following calculation and add the result to the `auth` parameter:
keccak384(key+addressIndex+bundleHash+apiKey)

Before you can call this command, you must initalize the secure element by calling the `initSecureElement' command. 

\textbf{Parameters}

\begin{tabular}{|c|c|c|}
\hline
Parameter & Type & Description \\
\hline
key & string & The memory address of the seed that owns the address \\
addressIndex & string & The index of the input transaction's address \\
bundleHash & integer & The bundle hash in the transaction's `bundle' field \\
securityLevel & integer & The security level of the input transaction's address \\
auth & string & The Keccak384 hash of the key, addressIndex, bundleHash, and the API key \\
\hline

\end{tabular}

\textbf{Example Request}

\begin{lstlisting}[language=json]
{
    "command":"signTransaction",
    "key": 0,
    "addressIndex": 0,
    "bundleHash": "...",
    "securityLevel": 2,
    "auth": "..."
}
\end{lstlisting}

\textbf{Example Responses}

\begin{lstlisting}[language=json]
{
    "code": 200,
    "command":"signTransaction",
    "trytes": ["....","....",...],
    "duration": 1800
}
\end{lstlisting}

\begin{lstlisting}[language=json]
{
    "code": 400,
    "command":"signTransaction",
    "error": "Error message"
}
\end{lstlisting}

\subsection{jsonDataTX}

Creates a zero-value transaction that contains the given JSON data in the `signatureMessageFragment` field.

This command returns the transaction trytes (including proof of work) of the zero-value transaction.

These trytes are ready for sending to a node.

\textbf{Example Request}
\begin{lstlisting}[language=json]
{
    "command": "jsonDataTX",
    "trunkTransaction":"...",
    "branchTransaction":"...",
    "minWeightMagnitude": 14,
    "tag":"...",
    "address":"...",
    "timestamp":1566907523000,
    "data":{"test":"myFirstCryptoCoreTransaction"}
}
\end{lstlisting}

\textbf{Examples Responses}

\begin{lstlisting}[language=json]
{
    "code": 200,
    "command": "jsonDataTX",
    "hash": "...9999",
    "trytes": ["....","....",...],
    "duration": 1800
}
\end{lstlisting}

\begin{lstlisting}[language=json]
{
    "code": 400,
    "command": "jsonDataTX",
    "error": "Error message"
}
\end{lstlisting}

\subsection{initSecureElement}
\label{apiInit}

Initializes the secure element so that the API can access the seed on it.

This command is a security measure that prevents attackers from removing the secure element, replacing it with another and reading the AES key from the RISC-V firmware. Before RISC-V shares the AES key with the secure element, you must call this command to prove that you know the key.

This command needs to be called only once.

\textbf{Example Request}

\begin{lstlisting}[language=json]
{
    "command": "initSecureElement",
    "key": "3780e63d4968ade5d822c013fcc323845d1b569fe705b60006feec145a0db1e3"
}
\end{lstlisting}

\textbf{Example Responses}
\begin{lstlisting}[language=json]
{
    "code": 200,
    "command": "initSecureElement",
    "duration": 1800
}
\end{lstlisting}

\begin{lstlisting}[language=json]
{
    "code": 400,
    "command": "initSecureElement",
    "error": "Error message"
}
\end{lstlisting}


\subsection{readFlashPage}
\label{apiReadFlashPage}

The readFlashPage command is used for reading pages (4kB) from QSPI flash memory. It is the same QSPI flash the FPGA uses when starting the configuration process. There is no (software) restriction other than valid page-numbers (0-4095) because the QSPI flash is not a secured memory. The output data is in base64 format.

\begin{lstlisting}[language=json]
{
    "command": "readFlashPage",
    "page": 0    
}
\end{lstlisting}

\textbf{Example Responses}
\begin{lstlisting}[language=json]
{
    "code": 200,
    "command": "readFlashPage",
    "duration": 12
    "data:"..b64.." 
}
\end{lstlisting}

\begin{lstlisting}[language=json]
{
    "code": 400,
    "command": "readFlashPage",
    "error": "Error message"
}
\end{lstlisting}

\subsection{writeFlashPage}
\label{apiWriteFlashPage}

The writeFlashPage command is used for writing pages (4kB) into the QSPI flash memory. \
It is the same QSPI flash the FPGA uses when starting the configuration process. \
This way, the soft-cpu can update the entire system by writing new bitstreams into the flash. \
This API-call is restricted. The `auth`-parameter is calculated this way:\

\textbf{Parameters}

\begin{tabular}{|c|c|l|}
\hline
Parameter & Type & Description \\
\hline
page & integer & page number in QSPI flash. Valid values are [0...4095] \\
\hline
data & string & 4kB data in Base64 format \\ 
\hline
auth & string & Checksum and authentication \\
     &        & auth = hexString(keccak384(page+data+apiKey))\\
\hline  
\end{tabular}

\begin{lstlisting}[language=json]
{
    "command": "writeFlashPage",
    "page": 0    
    "data": "..b64..",
    "auth": "....",
}
\end{lstlisting}

\textbf{Example Responses}
\begin{lstlisting}[language=json]
{
    "code": 200,
    "command": "writeFlashPage",
    "duration": 100
}
\end{lstlisting}

\begin{lstlisting}[language=json]
{
    "code": 400,
    "command": "writeFlashPage",
    "error": "Error message"
}
\end{lstlisting}

 
