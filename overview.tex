 
\newpage
\section{Overview}

The CryptoCore is IOTA hardware designed for applications that need fast, dedicated proof of work and a secure memory.

The device consists of an IOTA CryptoCore FPGA (ICCFPGA) module and a development board that doubles as a Raspberry Pi HAT, making it perfect for stand-alone applications and/or quick prototyping.

When developing on the CryptoCore, you can use the development board without any of the security features to debug the firmware, upload new firmware to ROM, and add new bitstreams to RAM or QSPI flash.

When you've finished developing, you can take advantage of the following built-in security features:

\paragraph{Secure Element}

The secure element is a tiny chip that's efficient at cryptographic functions, and robust against physical attacks.

This chip can store up to 8 seeds, therefore all communication with it can be encrypted, using rotating keys to prevent replay attacks.

\paragraph{CPU Protection}

Embedded on the CPU is a memory protection unit, which restricts access to memory, depending on the privilige level in which the code is running.

\paragraph{Bitstream Encryption}

The bitstream can be encrypted to prevent manipulation or extraction of program code and to prevent unauthorized code from being flashed onto the ICCFPGA module. 

\newpage 
\section{ICCFPGA Module}

\subsection{Overview}

The ICCFPGA module is a generic FPGA that, when loaded with the default ICCFPGA bitstream, provides security measures and hardware acceleration for the algorithms used in IOTA.

This section describes the ICCFPGA module as though it has been flashed with the ICCFPGA bitstream. However, you could program and compile custom code for this module.

As well as these features, the ICCFPGA module has a low power consumption, offers digital inputs and outputs, and exposes peripherals such as SPI or I2C.

\begin{center}
\includegraphics[width=6cm]{img/module-top.pdf}   
\hspace{1cm}
\includegraphics[width=6cm]{img/module-bot.pdf} 
\end{center}
  
\subsection{Technical Data}
\begin{itemize}
 \item XC7S50 Spartan 7 Series FPGA
 \item 16 MB Flash (used for configuration, and accessible from the soft CPU)
 \item ATECC608A secure element
 \item RISC-V compatible soft CPU with 128 kB ROM, 128 kB RAM, 4 kB Supervisor RAM, running at 100 MHz
 \item Hardware accelerators for proof of work (Curl-P81), Keccak384, Troika (all single cycle per hashing round), and ternary/binary conversions.
 \item JTAG for ICCFPGA
 \item JTAG for RISC-V (full debugging support including semihosting)
 \item 19 GPIOs (alternate functions: 2 SPI-Master, I2C, and UART)
 \item 16 spare GPIOs (not used by the ICCFPGA)
 \item FPGA control signals* on the card connector
\end{itemize}

*: Control signals: /INIT, /PROGRAM (from V1.2), CLK\_HOLD (for stopping the internal phase-locked loop)

\subsection{Pinout}

The ICCFPGA module uses a mini-PCIe connector (and its pin numbering) with custom signals on the connector. 

%\begin{figure}
%\center
%\includegraphics[width=10cm]{fpga-fpga.pdf}
%\end{figure}
\begin{center}
\begin{tabular}{|P{3cm}|c|c|c|c|}
\hline
Module Pin-Number & Name & Function & Alt.Func & BGA-Pad \\
\hline
1 & RV\_TCK & RISC-V JTAG TCK & & H2 \\
\hline
3 & RV\_TDO & RISC-V JTAG TDO & & J1 \\
\hline
5 & RV\_TDI & RISC-V JTAG TDI & & J2 \\
\hline
8 & IO0 & GPIO0 & MOSI0 & L1 \\
\hline
10 & IO1 & GPIO1 & MISO0 & L2 \\
\hline
11 & RV\_RESET & RISC-V reset & & M1 \\
\hline
12 & IO2 & GPIO2 & SCK0 & P2 \\
\hline
13 & RV\_TMS & RISC-V TMS & & M2 \\
\hline
14 & IO3 & GPIO3 & SS0 & M4 \\
\hline
16 & IO4 & GPIO4 & SS1 & P3 \\
\hline
17 & IO7 & GPIO7 & UART TXD & N4 \\
\hline
19 & IO8 & GPIO8 & UART RXD & P4 \\
\hline
20 & FPGA\_TCK & FPGA JTAG TCK & & \\
\hline
22 & FPGA\_TMS & FPGA JTAG TMS & & \\
\hline
23 & FPGA\_TDO & FPGA JTAG TDO & & \\
\hline
25 & FPGA\_TDI & FPGA JTAG TDI & & \\
\hline
28 & IO9 & GPIO9 & & P10 \\
\hline
30 & IO10 & GPIO10 & & N10 \\
\hline
31 & IO11 & GPIO11 & & M10 \\
\hline
32 & IO12 & GPIO12 & & P11 \\
\hline
33 & IO13 & GPIO13 & & N11 \\
\hline
36 & IO14 & GPIO14 & & M11 \\
\hline
38 & IO5 & GPIO5 & SCL0 & P12 \\
\hline
42 & IO6 & GPIO6 & SDA0 & M12 \\
\hline
44 & IO15 & GPIO15 & & P13 \\
\hline
45 & IO16 & GPIO16 & & M13 \\
\hline
46 & IO17 & GPIO17 & & N14 \\
\hline
47 & IO18 & GPIO18 & & M14 \\
\hline
49 & CLK\_HOLD & FPGA hold clock & & L12 \\
\hline
51 & /INIT & FPGA init & &  \\
\hline
48 & /PROGRAM* & FPGA program (>=V1.2) & &  \\
\hline
6, 7 & NC & Not Connected & & \\
\hline
2, 24, 29, 39, 41, 52 & +3.3V & & & \\
\hline
4, 9, 15, 18, 21, 26, 27, 29, 34, 35, 37, 40, 43, 50 & GND & & & \\
\hline

\end{tabular}
\end{center}

*: Pin 48 can be used for triggering the ICCFPGA reconfiguration. This pin is useful for rebooting the ICCFPGA module after updating the bitstream in the QSPI flash. Otherwise, a power-cycle is currently needed. This functionality also is available in software running on the RISC-V soft-cpu for triggering reboots after bitstream-updates.
